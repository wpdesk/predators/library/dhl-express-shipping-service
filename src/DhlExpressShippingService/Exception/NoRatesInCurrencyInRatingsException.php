<?php

namespace WPDesk\DhlExpressShippingService\Exception;

use WPDesk\AbstractShipping\Shop\ShopSettings;

/**
 * Exception thrown when switcher is not accepted.
 *
 * @package WPDesk\DhlExpressShippingService\Exception
 */
class NoRatesInCurrencyInRatingsException extends \RuntimeException {

	public function __construct() {
		$message = __( 'The shop\'s currency is other than set on the DHL Express account.', 'dhl-express-shipping-service' );
		parent::__construct( $message );
	}

}

