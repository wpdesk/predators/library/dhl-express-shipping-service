<?php

namespace WPDesk\DhlExpressShippingService\DhlApi\RestApi;

use DHL\Client\Web;
use DHL\Entity\AM\GetQuote;
use DHL\Entity\AM\GetQuoteResponse;
use Octolize\DhlExpress\RestApi\MyDHL;
use Octolize\DhlExpress\RestApi\Services\RateService;
use Psr\Log\LoggerInterface;
use WPDesk\AbstractShipping\Exception\RateException;
use WPDesk\DhlExpressShippingService\DhlApi\Sender;

/**
 * Send request to DHL Express API
 *
 * @package WPDesk\DhlExpressShippingService\DhlApi
 */
class RestApiDhlSender implements Sender
{

	/**
	 * Logger
	 *
	 * @var LoggerInterface
	 */
	private $logger;


	/**
	 * Is testing?
	 *
	 * @var bool
	 */
	private bool $is_testing;

	private string $api_key;

	private string $api_secret;

	/**
	 * DhlSender constructor.
	 *
	 * @param LoggerInterface $logger Logger.
	 * @param bool $is_testing Is testing?.
	 */
	public function __construct(LoggerInterface $logger, string $api_key, string $api_secret, bool $is_testing = true)
	{
		$this->logger     = $logger;
		$this->api_key = $api_key;
		$this->api_secret = $api_secret;
		$this->is_testing = $is_testing;
	}

	/**
	 * Send request.
	 *
	 * @param RateService $request DHL request.
	 *
	 * @return array
	 *
	 * @throws \Exception
	 */
	public function send($request)
	{
		$mydhl = new MyDHL($this->api_key, $this->api_secret, $this->is_testing );

		$this->logger->info(
			'API request',
			[
				'content' => json_encode( $request->prepareQuery(), JSON_PRETTY_PRINT ),
				'is_testing' => json_encode( $this->is_testing ),
			]
		);

		try {
			$request->setClient($mydhl->getClient());
			$rates = $request->getRates();
		} catch (\Exception $e) {
			throw new RateException( $e->getMessage(), [], $e->getCode(), $e );
		}

		$this->logger->info(
			'API response',
			[
				'content' => json_encode( $request->getLastRawResponse(), JSON_PRETTY_PRINT ),
			]
		);

		return $rates;
	}

}
