<?php

namespace WPDesk\DhlExpressShippingService\DhlApi;

use DHL\Entity\AM\GetQuote;
use DHL\Entity\AM\GetQuoteResponse;

/**
 * Sender class interface.
 *
 * @package WPDesk\DhlExpressShippingService\DhlApi
 */
interface Sender {

	/**
	 * Send request.
	 *
	 * @param GetQuote $request Request.
	 *
	 * @return GetQuoteResponse
	 */
	public function send( $request );

}
